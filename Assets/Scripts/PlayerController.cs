﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using Photon.Pun;
using Photon.Realtime;
using System.IO;
using UnityEngine.UI;

public class PlayerController : MonoBehaviourPunCallbacks
{
    [SerializeField] TMP_Text nameText;
    
    bool isActive;

    float moveSpeed = 10;
    float moveDelay = 1;
    int direction;

    PhotonView view;

    public void Init()
    {
        isActive = true;
        transform.localPosition = GetRandomPlayerPosition();
        nameText.text = PhotonNetwork.NickName;
    }

    [PunRPC] void RPCSetCharacter(string name, string colorString)
    {
        nameText.text = name;
        Color color;
        //if (ColorUtility.TryParseHtmlString(colorString, out color)) {
        //    GetComponent<SpriteRenderer>().color = color;
        //    nameText.color = color;
        //}

        color = GameController.instance.GetColorFuckedUpStyle(colorString);
        GetComponent<SpriteRenderer>().color = color;
        nameText.color = color;
    }

    [PunRPC] void RPCSendChat(string name, string color, string chat)
    {
        GameController.instance.SendChat(name, color, chat);
    }

    [PunRPC] void RPCStartQuiz(string quest, string answer)
    {
        GameController.instance.StartQuiz(quest, answer);
    }

    [PunRPC] void RPCSendAnswer(string name, string color, string answer)
    {
        GameController.instance.AnswerQuiz(name, color, answer);
    }

    [PunRPC] void RPCSendLine(string line)
    {
        GameController.instance.CreateLineFromBack(line);
    }

    Vector2 GetRandomPlayerPosition()
    {
        return new Vector3(Random.Range(-GetMaxWorld() + 1f, GetMaxWorld() - 1f), Random.Range(-Camera.main.orthographicSize + 1, Camera.main.orthographicSize - 1), 0);
    }

    float GetMaxWorld()
    {
        return Camera.main.orthographicSize / (float)((float)Screen.height / (float)Screen.width);
    }

    // Start is called before the first frame update
    void Start()
    {
        view = GetComponent<PhotonView>();
        //string colorString = ColorUtility.ToHtmlStringRGB(color);
        //Debug.LogError(colorString);
        if (view.IsMine)
        {
            view.RPC("RPCSetCharacter", RpcTarget.AllBuffered, PhotonNetwork.NickName, PhotonNetwork.LocalPlayer.CustomProperties["color"].ToString());
        }
        moveDelay = 1;
        direction = Random.Range(0, 4);
    }

    // Update is called once per frame
    void Update()
    {
        if (!isActive)
        {
            return;
        }

        if (moveDelay > 0)
        {
            moveDelay -= Time.deltaTime;
            Move(direction);

            if (moveDelay <= 0)
            {
                moveDelay = 1;
                direction = Random.Range(0, 4);
            }

        }
    }

    void Move(int code)
    {

        if (code == 0)
        {
            transform.localPosition = new Vector2(transform.localPosition.x + (moveSpeed * Time.deltaTime), transform.localPosition.y);
        }
        else if (code == 1)
        {
            transform.localPosition = new Vector2(transform.localPosition.x - (moveSpeed * Time.deltaTime), transform.localPosition.y);
        }
        else if (code == 2)
        {
            transform.localPosition = new Vector2(transform.localPosition.x, transform.localPosition.y + (moveSpeed * Time.deltaTime));
        }
        else if (code == 3)
        {
            transform.localPosition = new Vector2(transform.localPosition.x, transform.localPosition.y - (moveSpeed * Time.deltaTime));
        }

        if (transform.localPosition.x > GetMaxWorld() - 1)
        {
            transform.localPosition = new Vector2(GetMaxWorld() - 1, transform.localPosition.y);
        }

        if (transform.localPosition.x < -GetMaxWorld() + 1)
        {
            transform.localPosition = new Vector2(-GetMaxWorld() + 1, transform.localPosition.y);
        }

        if (transform.localPosition.y > Camera.main.orthographicSize - 1)
        {
            transform.localPosition = new Vector2(transform.localPosition.x, Camera.main.orthographicSize - 1);
        }

        if (transform.localPosition.y < -Camera.main.orthographicSize + 1)
        {
            transform.localPosition = new Vector2(transform.localPosition.x, -Camera.main.orthographicSize + 1);
        }
    }
}
