﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DrawAFuckingLine : MonoBehaviour
{
    [SerializeField] GameObject lineSample;

    GameObject activeLine;
    LineRenderer activeLineRenderer;

    List<Vector2> fingPos;
    List<LineRenderer> lineList;

    void CreateAFuckingLine()
    {
        activeLine = Instantiate(lineSample, Vector2.zero, Quaternion.identity);
        activeLineRenderer = activeLine.GetComponent<LineRenderer>();
        
        lineList.Add(activeLineRenderer);
        
        fingPos.Clear();
        fingPos.Add(Camera.main.ScreenToWorldPoint(Input.mousePosition));
        fingPos.Add(Camera.main.ScreenToWorldPoint(Input.mousePosition));
        activeLineRenderer.SetPosition(0, fingPos[0]);
        activeLineRenderer.SetPosition(1, fingPos[1]);
    }

    void DrawLine(Vector2 position)
    {
        fingPos.Add(position);
        activeLineRenderer.positionCount++;
        activeLineRenderer.SetPosition(activeLineRenderer.positionCount-1, position);
    }

    // Start is called before the first frame update
    void Start()
    {
        fingPos = new List<Vector2>();
        lineList = new List<LineRenderer>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            CreateAFuckingLine();
        }

        if (Input.GetMouseButton(0))
        {
            Vector2 fingerPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            if (Vector2.Distance(fingerPos, fingPos[fingPos.Count - 1]) > 0.1f)
            {
                DrawLine(fingerPos);
            }
        }

        if (Input.GetMouseButtonUp(0))
        {
            GameController.instance.SendGodDamnLine();
        }
    }

    public string GetLineString()
    {
        string temp = "";
        for (int i = 0; i < activeLineRenderer.positionCount; i++)
        {
            string line = "@";
            line += "x" + activeLineRenderer.GetPosition(i).x;
            line += "y" + activeLineRenderer.GetPosition(i).y;

            temp += line;
        }

        //Debug.LogError(temp);
        return temp;
    }

    public void MakeLineFromString(string line)
    {
        string[] aha = line.Split('@');

        LineRenderer lineRen = Instantiate(lineSample, Vector2.zero, Quaternion.identity).GetComponent<LineRenderer>();
        lineRen.positionCount = aha.Length - 1;
        for (int i = 1; i < aha.Length; i++)
        {
            //Debug.LogError(aha[i]);
            //Debug.LogError(aha[i].Substring(aha[i].IndexOf("y") + 1, aha[i].Length - 1 - aha[i].IndexOf("y")));
            float x = float.Parse(aha[i].Substring(aha[i].IndexOf("x") + 1, aha[i].IndexOf("y") - 1 - aha[i].IndexOf("x")));
            float y = float.Parse(aha[i].Substring(aha[i].IndexOf("y") + 1, aha[i].Length - 1 - aha[i].IndexOf("y")));
            lineRen.SetPosition(i - 1, new Vector3(x, y, 0));
        }
    }
}