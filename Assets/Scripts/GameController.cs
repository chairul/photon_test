﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using System.IO;
using Photon.Realtime;
using UnityEngine.UI;

public class GameController : MonoBehaviourPunCallbacks
{
    public static GameController instance;
    private void Awake()
    {
        instance = this;
    }

    PhotonView myPlayer;
    [SerializeField] Transform chatParent;
    [SerializeField] InputField chatInput;
    [SerializeField] Button chatButton;
    [SerializeField] Text roomText;
    [SerializeField] GameObject quizButton;

    [SerializeField] GameObject qPanel;
    [SerializeField] Text qText;
    [SerializeField] GameObject aGuruPanel;
    [SerializeField] GameObject aStudentPanel;
    [SerializeField] GameObject rStudentPanel;
    [SerializeField] Text rStudentText;
    [SerializeField] Transform aParent;
    [SerializeField] Transform bParent;
    [SerializeField] Transform cParent;
    [SerializeField] Transform dParent;
    [SerializeField] GameObject textAnswerSample;

    [SerializeField] GameObject drawButton;
    [SerializeField] GameObject drawPanel;
    [SerializeField] GameObject pencil;
    [SerializeField] DrawAFuckingLine goddamnLineManager;

    public string answer;
    public bool isDrawing;

    public void SendChat(string name, string color, string chat)
    {
        GameObject text = PhotonNetwork.Instantiate(Path.Combine("Prefabs", "textSample"), Vector3.zero, Quaternion.identity);
        text.transform.SetParent(chatParent);

        Color colorOri = GetColorFuckedUpStyle(color);
        string colorString = ColorUtility.ToHtmlStringRGB(colorOri);

        text.GetComponent<Text>().text = string.Format("<color=#{0}>[{1}]</color> {2}", colorString, name, chat);
        chatButton.interactable = true;
        chatInput.text = "";
    }

    public void SendQuiz()
    {
        string quest = "";
        for (int i = 0; i < Random.Range(3, 10); i++)
        {
            quest += "bla...";
        }
        quest += "?";

        string answer = "";
        List<string> an = new List<string>() { "A", "B", "C", "D" };
        answer = an[Random.Range(0, an.Count)];

        if (myPlayer.IsMine)
        {
            myPlayer.RPC("RPCStartQuiz", RpcTarget.AllBuffered, quest, answer);
        }
    }

    public void StartQuiz(string quest, string answer)
    {
        quizButton.SetActive(false);

        qPanel.SetActive(true);
        qText.text = quest;

        this.answer = answer;

        aGuruPanel.SetActive(bool.Parse(PhotonNetwork.LocalPlayer.CustomProperties["guru"].ToString()));
        aStudentPanel.SetActive(!bool.Parse(PhotonNetwork.LocalPlayer.CustomProperties["guru"].ToString()));
    }

    public void AnswerQuiz(string answer)
    {
        aStudentPanel.SetActive(false);
        rStudentPanel.SetActive(true);

        rStudentText.text = "Jawaban " + answer + " = " + (this.answer == answer ? "BENAR" : "SALAH");
        SendAnswer(answer);
    }

    void SendAnswer(string answer)
    {
        if (myPlayer.IsMine)
        {
            myPlayer.RPC("RPCSendAnswer", RpcTarget.AllBuffered, PhotonNetwork.NickName, PhotonNetwork.LocalPlayer.CustomProperties["color"].ToString(), answer);
        }
    }

    public void AnswerQuiz(string name, string color, string answer)
    {
        Transform parent;
        if (answer == "A")
        {
            parent = aParent;
        }
        else if (answer == "B")
        {
            parent = bParent;
        }
        else if (answer == "C")
        {
            parent = cParent;
        }
        else
        {
            parent = dParent;
        }

        Text text = Instantiate(textAnswerSample, parent).GetComponent<Text>();
        text.text = name;
        text.color = GetColorFuckedUpStyle(color);
    }

    public void StartDraw()
    {
        drawButton.SetActive(false);
        drawPanel.SetActive(true);
        pencil.SetActive(true);

        isDrawing = true;
    }

    public void SendGodDamnLine()
    {
        string lineString = goddamnLineManager.GetLineString();
        if (myPlayer.IsMine)
        {
            myPlayer.RPC("RPCSendLine", RpcTarget.AllBuffered, lineString);
        }
    }

    public void CreateLineFromBack(string lineString)
    {
        if (bool.Parse(PhotonNetwork.LocalPlayer.CustomProperties["guru"].ToString()))
        {
            return;
        }

        drawPanel.SetActive(true);
        goddamnLineManager.MakeLineFromString(lineString);
    }


    // Start is called before the first frame update
    void Start()
    {
        roomText.text = TestControlLobby.myRoom;

        CreatePlayer();
        StartCoroutine(RefreshChat());

        quizButton.SetActive(bool.Parse(PhotonNetwork.LocalPlayer.CustomProperties["guru"].ToString()));
        drawButton.SetActive(bool.Parse(PhotonNetwork.LocalPlayer.CustomProperties["guru"].ToString()));

        drawPanel.SetActive(false);
        pencil.SetActive(false);

        isDrawing = false;
    }

    void CreatePlayer()
    {
        GameObject player = PhotonNetwork.Instantiate(Path.Combine("Prefabs", "Player"), Vector3.zero, Quaternion.identity);

        if (player.GetComponent<PhotonView>().IsMine)
        {
            myPlayer = player.GetComponent<PhotonView>();
            player.GetComponent<PlayerController>().Init();
        }
    }

    public void SendChat()
    {
        if (string.IsNullOrEmpty(chatInput.text))
        {
            return;
        }

        chatButton.interactable = false;
        chatParent.GetComponent<ContentSizeFitter>().enabled = false;

        if (myPlayer.IsMine)
        {
            myPlayer.RPC("RPCSendChat", RpcTarget.AllBuffered, PhotonNetwork.NickName, PhotonNetwork.LocalPlayer.CustomProperties["color"].ToString(), chatInput.text);
        }
    }

    IEnumerator RefreshChat()
    {
        while (true)
        {
            yield return null;
            chatParent.GetComponent<ContentSizeFitter>().enabled = false;
            yield return null;
            chatParent.GetComponent<ContentSizeFitter>().enabled = true;
        }
    }

    // Update is called once per frame
    void Update()
    {

    }

    public Color GetColorFuckedUpStyle(string colorString)
    {
        float r = float.Parse(colorString.Substring(colorString.IndexOf("r") + 1, colorString.IndexOf("g") - colorString.IndexOf("r") - 1));
        float g = float.Parse(colorString.Substring(colorString.IndexOf("g") + 1, colorString.IndexOf("b") - colorString.IndexOf("g") - 1));
        float b = float.Parse(colorString.Substring(colorString.IndexOf("b") + 1, colorString.IndexOf("a") - colorString.IndexOf("b") - 1));

        return new Color(r, g, b);
    }
}
