﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using Photon.Realtime;
using UnityEngine.UI;

public class TestControlLobby : MonoBehaviourPunCallbacks
{
    public static Color playerColor;
    public static string myRoom;

    [SerializeField] GameObject startButton;
    [SerializeField] GameObject cancelButton;
    [SerializeField] int roomSize;
    [SerializeField] Text matchTextButton;
    [SerializeField] InputField roomInput;

    bool isGuru;

    private void Awake()
    {
        startButton.SetActive(false);
        cancelButton.SetActive(false);
    }

    // Start is called before the first frame update
    void Start()
    {
        isGuru = true;
        if (PhotonNetwork.LocalPlayer.CustomProperties.ContainsKey("guru"))
        {
            PhotonNetwork.LocalPlayer.CustomProperties["guru"] = isGuru.ToString();
        }
        else
        {
            PhotonNetwork.LocalPlayer.CustomProperties.Add("guru", isGuru.ToString());
        }
    }

    // Update is called once per frame
    void Update()
    {

    }

    public override void OnConnectedToMaster()
    {
        PhotonNetwork.AutomaticallySyncScene = true;
        startButton.SetActive(true);
    }
    public void Match()
    {
        if (!isGuru)
        {
            if (string.IsNullOrEmpty(roomInput.text))
            {
                return;
            }
            else
            {
                myRoom = roomInput.text;
                PhotonNetwork.JoinRoom(roomInput.text);
            }
        }
        else
        {
            CreateNewRoom();
        }

        startButton.SetActive(false);
        cancelButton.SetActive(true);
    }

    public override void OnJoinRandomFailed(short returnCode, string message)
    {
        Debug.Log("Failed to Match!");
    }

    void CreateNewRoom()
    {
        Debug.Log("Creating New Room");
        int roomNumber = Random.Range(0, 99999);
        myRoom = roomNumber.ToString();
        RoomOptions roomProp = new RoomOptions()
        {
            IsVisible = true,
            IsOpen = true,
            MaxPlayers = (byte)roomSize
        };
        PhotonNetwork.CreateRoom(roomNumber.ToString(), roomProp);
    }

    public override void OnCreateRoomFailed(short returnCode, string message)
    {
        Debug.Log("Failed to Create Room!");
        CreateNewRoom();
    }

    public void QuickCancel()
    {
        startButton.SetActive(true);
        cancelButton.SetActive(false);
        PhotonNetwork.LeaveRoom();
    }

    public void NameInput(string name)
    {
        PhotonNetwork.NickName = name;
        playerColor = Random.ColorHSV();
        string colorString = string.Format("r{0}g{1}b{2}a", playerColor.r, playerColor.g, playerColor.b);
        if (PhotonNetwork.LocalPlayer.CustomProperties.ContainsKey("color"))
        {
            PhotonNetwork.LocalPlayer.CustomProperties["color"] = colorString;
        }
        else
        {
            PhotonNetwork.LocalPlayer.CustomProperties.Add("color", colorString);
        }
    }

    public void GuruInput(bool isGuru)
    {
        this.isGuru = isGuru;
        if (PhotonNetwork.LocalPlayer.CustomProperties.ContainsKey("guru"))
        {
            PhotonNetwork.LocalPlayer.CustomProperties["guru"] = isGuru.ToString();
        }
        else
        {
            PhotonNetwork.LocalPlayer.CustomProperties.Add("guru", isGuru.ToString());
        }
        matchTextButton.text = isGuru ? "Create Room" : "Join Room";

        roomInput.gameObject.SetActive(!isGuru);
    }
}
